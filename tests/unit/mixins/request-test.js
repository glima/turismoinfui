import Ember from 'ember';
import RequestMixin from 'ui/mixins/request';
import { module, test } from 'qunit';

module('Unit | Mixin | request');

// Replace this with your real tests.
test('it works', function(assert) {
  let RequestObject = Ember.Object.extend(RequestMixin);
  let subject = RequestObject.create();
  assert.ok(subject);
});
