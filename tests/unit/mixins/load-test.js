import Ember from 'ember';
import LoadMixin from 'ui/mixins/load';
import { module, test } from 'qunit';

module('Unit | Mixin | load');

// Replace this with your real tests.
test('it works', function(assert) {
  let LoadObject = Ember.Object.extend(LoadMixin);
  let subject = LoadObject.create();
  assert.ok(subject);
});
