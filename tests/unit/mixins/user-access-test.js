import Ember from 'ember';
import UserAccessMixin from 'ui/mixins/user-access';
import { module, test } from 'qunit';

module('Unit | Mixin | user access');

// Replace this with your real tests.
test('it works', function(assert) {
  let UserAccessObject = Ember.Object.extend(UserAccessMixin);
  let subject = UserAccessObject.create();
  assert.ok(subject);
});
