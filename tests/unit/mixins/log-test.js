import Ember from 'ember';
import LogMixin from 'ui/mixins/log';
import { module, test } from 'qunit';

module('Unit | Mixin | log');

// Replace this with your real tests.
test('it works', function(assert) {
  let LogObject = Ember.Object.extend(LogMixin);
  let subject = LogObject.create();
  assert.ok(subject);
});
