import Ember from 'ember';
import ListingMixin from 'ui/mixins/listing';
import { module, test } from 'qunit';

module('Unit | Mixin | listing');

// Replace this with your real tests.
test('it works', function(assert) {
  let ListingObject = Ember.Object.extend(ListingMixin);
  let subject = ListingObject.create();
  assert.ok(subject);
});
