import Ember from 'ember';
import ConfRoutineConfMixin from 'ui/mixins/conf/routine-conf';
import { module, test } from 'qunit';

module('Unit | Mixin | conf/routine conf');

// Replace this with your real tests.
test('it works', function(assert) {
  let ConfRoutineConfObject = Ember.Object.extend(ConfRoutineConfMixin);
  let subject = ConfRoutineConfObject.create();
  assert.ok(subject);
});
