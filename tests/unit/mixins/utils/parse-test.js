import Ember from 'ember';
import UtilsParseMixin from 'ui/mixins/utils/parse';
import { module, test } from 'qunit';

module('Unit | Mixin | utils/parse');

// Replace this with your real tests.
test('it works', function(assert) {
  let UtilsParseObject = Ember.Object.extend(UtilsParseMixin);
  let subject = UtilsParseObject.create();
  assert.ok(subject);
});
