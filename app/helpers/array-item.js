import Ember from 'ember';

export default Ember.Helper.helper(function (ar, n) {
  let array = ar[0];
  let index = ar[1];
  return array !== undefined && array[index] !== undefined ? array[index] : undefined;
});
