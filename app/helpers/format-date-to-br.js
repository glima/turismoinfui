import Ember from 'ember';
import Parse from '../mixins/utils/parse';

export default Ember.Helper.extend(Parse,  {
    compute: function(params /*, hash*/) {
      return this.parseStringDateToBr(params[0]);
    }
});