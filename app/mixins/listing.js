import Ember from 'ember';
import ENV from '../config/environment';
import Parse from '../mixins/utils/parse';
import Request from '../mixins/request';
import {isAjaxError, isNotFoundError, isForbiddenError, isBadRequestError} from 'ember-ajax/errors';

export default Ember.Mixin.create(Parse, Request, {
    rootUrl: ENV.rootURL,
    appController: Ember.inject.controller('app'),
    
    dialog: {show: false, msg: '', title: ''},
    queryParams: ['search', 'page'],
    search: null,
    limit: 15,
    limitOptions: [15],
    pages: [1],
    page: 1,
    totalPages: 15,
    url: '',

    filterDialog: false,
    filter: null,
    list: null, 

    init() {
        this._super();
        this.resetFilter();
        this.set('model', {});
    },
    
    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i].title + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }            
    },

    resetFilter() {
        this.set('filter', {});
        this.set('search', null);
        this.set('page', 1);
        this.set('list', null);
    },

    makeRequest() {
        var context = this;
        let contextApp = this.get('appController');
        contextApp.enableLoading(); 
        if(this.get('search') == null) {
            this.ajaxGet(this.get('url') + '?page=' + this.get('page'), contextApp)
            .then((response) => {
                contextApp.disableLoading();
                context.set('model.item', response);
                context.set('totalPages', response.total);
            });
        } else {
            this.ajaxGet(this.get('url') + '?page=' + this.get('page') 
                + '&search=' + this.get('search'), context)
            .then((response) => {
                contextApp.disableLoading();
                context.set('model.item', response);
                context.set('totalPages', response.total);
            });
        }
    },

    actions: {

        exportCsv(fileName) {
            var context = this;
            let contextApp = this.get('appController');
            contextApp.enableLoading(); 
            if(this.get('search') == null) {
                return this.ajaxCsv(this.get('url')+'/file', fileName, contextApp)
                .then((response) => {
                    if(response) {
                        contextApp.disableLoading();
                    }
                }).catch((error) => {
                    contextApp.disableLoading();
                    if(isBadRequestError(error)) {
                        this.showDialogScreen(error.errors, true);
                        return;
                    }
                });
            } else {
                return this.ajaxCsv(this.get('url') + '/file?search=' + this.get('search'), fileName, context)
                .then((response) => {
                    if(response) {
                        contextApp.disableLoading();
                    }
                }).catch((error) => {
                    contextApp.disableLoading();
                    if(isBadRequestError(error)) {
                        this.showDialogScreen(error.errors, true);
                        return;
                    }
                });
            }
        },

        updateItem(id, item) {
            var context = this;
            let contextApp = this.get('appController');   
            contextApp.enableLoading(); 
            
            this.ajaxPut(this.get('url') + '/' + id, { data: JSON.stringify(item) }, contextApp)
            .then((response) => {
                if(response) {
                    contextApp.disableLoading();
                    this.showDialogScreen("Atualização realizada com sucesso.", false);
                    this.showList();
                    this.makeRequest();
                }
            }).catch((error) => {
                contextApp.disableLoading();
                if(isBadRequestError(error)) {
                    this.showDialogScreen(error.errors, true);
                    return;
                }

                if(isNotFoundError(error)) {
                    this.showDialogScreen("Item não encontrado", false);
                }
            });
        },

        showList(list) {
            if (typeof JSON.clone !== "function") {
                JSON.clone = function(list) {
                    return JSON.parse(JSON.stringify(list));
                };
            }

            this.set('listCopy', list);
            this.set('list', JSON.clone(list));
        },

        incrementPage() {
            if(this.get('page') < this.get('totalPages') / 15) {
                this.set('page', this.get('page') + 1);
                this.makeRequest();
            }            
        },

        decrementPage() {
            if(this.get('page') != 1) {
                this.set('page', this.get('page') - 1);
                this.makeRequest();
            }
        },
        
        openDialogSearch() {
            this.toggleProperty('filterDialog');
            this.resetFilter();
        },

        requestFilter() {
            let filter = '';
            let property = this.get('filter');
            let keys = Object.keys(property)

            for(let i = 0; i < keys.length; i++) {
                let value = property[keys[i]].value;
                if(value) {
                    if(property[keys[i]].type == 'date') {
                        filter += keys[i] + property[keys[i]].op + this.parseStringDateNoDash(value) + ",";
                    } else {
                        filter += keys[i] + property[keys[i]].op + value + ",";
                    }                    
                }                
            }

            if(filter != '') {
                filter = filter.substring(0, filter.length - 1);
            }

            this.resetFilter();

            if(filter == '') {
                this.set('search', null);
            } else {
                this.set('search', filter);
            }

            this.makeRequest();
            this.toggleProperty('filterDialog');
        },
        
        closeDialog() {
            this.set('dialog.show', false);
        }
    }
});
