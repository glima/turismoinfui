import Ember from 'ember';

export default Ember.Mixin.create({

    status: true,

    statusOpposite: Ember.computed.not('status'),

    visibleDisable: Ember.computed("optionsSelected", "status", {
        get() {
            if(this.get("status") || this.get("status") == undefined) {
                if(this.get("optionsSelected") != undefined && this.get("optionsSelected").length > 1) {
                    return true;
                } else {
                    return false;
                }
            }

            return true;
        }
    }),

    visible: Ember.computed("optionsSelected", "status", {
        get(key) {
            if(this.get("status") || this.get("status") == undefined) {
                if(this.get("optionsSelected") != undefined && this.get("optionsSelected").length > 1) { 
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        },
        set(key, value) {
            return value;
        }
    }),

    requiredDisable: Ember.computed("status", {
        get() {
            if(this.get("status") || this.get("status") == undefined) {
                return false;
            }

            return true;
        }
    }),

    confirmDisable: Ember.computed("status", {
        get() {
            if(this.get("status") || this.get("status") == undefined) {
                return false;
            }

            return true;
        }
        
    }),

    statusChange: Ember.observer("status", (context, prop) => {
        if(context.normalizeValues) {
            context.normalizeValues();
        }        
    })
});
