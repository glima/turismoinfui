import Ember from 'ember';

export default Ember.Mixin.create({
    
    changeTitle(title, icon) {
        this.controllerFor('app').changeTitle(title, icon);
    },

    beforeModel() {
        this._super();
        this.enableLoading();
    },

    renderTemplate(controller, model) {
        this._super(controller, model);
        this.disableLoading();
        
        this.controllerFor('app').set('administrator', this.controllerFor('app').isAdministrator());
        this.controllerFor('app').set('intergado', this.controllerFor('app').isIntergado());
        this.controllerFor('app').defineUserName();

        if(this.routeName != 'app.index' && controller.isAdministrator) {
            controller.set('administrator', controller.isAdministrator());
            controller.set('intergado', controller.isIntergado());
        }      
    },

    enableLoading() {
        this.controllerFor('app').enableLoading();
    },

    disableLoading() {
        this.controllerFor('app').disableLoading();
    }
});
