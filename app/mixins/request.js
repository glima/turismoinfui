import Ember from 'ember';
import {isForbiddenError, isServerError, isUnauthorizedError} from 'ember-ajax/errors';

export default Ember.Mixin.create({
    ajax: Ember.inject.service(),

    ajaxPut(url, data, appController) {
        return this.get('ajax').put(url, data)
        .catch((errorResponse) => {
            this.errorCallback(errorResponse, appController);
            throw errorResponse;
        });
    },

    ajaxPatch(url, data, appController) {
        return this.get('ajax').patch(url, data)
        .catch((errorResponse) => {
            this.errorCallback(errorResponse, appController);
            throw errorResponse;
        });
    },

    ajaxPost(url, data, appController) {
        return this.get('ajax').post(url, data)
        .catch((errorResponse) => {
            this.errorCallback(errorResponse, appController);
            throw errorResponse;
        });
    },

    ajaxDelete(url, appController) {
        return this.get('ajax').delete(url)
        .catch((errorResponse) => {
            this.errorCallback(errorResponse, appController);
            throw errorResponse;
        });
    },

    ajaxGet(url, appController) {
        return this.get('ajax').request(url)
        .catch((errorResponse) => {
            this.errorCallback(errorResponse, appController);
            throw errorResponse;
        });
    },

    ajaxCsv(url, fileName, appController) {
        return this.get('ajax').request(url, {dataType: 'text', responseType : 'blob'})
        .then((csv) => {
            var data = encodeURI(csv);
    
            this.download(csv, fileName);
            return csv;
        })
        .catch((errorResponse) => {
            this.errorCallback(errorResponse, appController);
            throw errorResponse;
        });
    },

    download(csvString, fileName) {
        var blob = new Blob([csvString]);
        if (window.navigator.msSaveOrOpenBlob)  // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
            window.navigator.msSaveBlob(blob, fileName + ".csv");
        else
        {
            var a = window.document.createElement("a");
            a.href = window.URL.createObjectURL(blob, {type: "text/plain"});
            a.download = fileName + ".csv";
            document.body.appendChild(a);
            a.click();  // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
            document.body.removeChild(a);
        }
    },

    ajaxGetPdf(url, appController) {
        return this.get('ajax').request(url, {dataType: "text"})
        .then((response) => {
            //return new Uint8Array(response);
            //return URL.createObjectURL(response);
            return response;
        })
        .catch((errorResponse) => {
            this.errorCallback(errorResponse, appController);
            throw errorResponse;
        });
    },

    base64ToUint8Array(base64) {//base64 is an byte Array sent from server-side
        var raw = atob(base64); //This is a native function that decodes a base64-encoded string.
        var uint8Array = new Uint8Array(new ArrayBuffer(raw.length));
        
        for (var i = 0; i < raw.length; i++) {
            uint8Array[i] = raw.charCodeAt(i);
        }

        return uint8Array;
    },

    base64Encode(str) {
        var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var out = "", i = 0, len = str.length, c1, c2, c3;
        while (i < len) {
            c1 = str.charCodeAt(i++) & 0xff;
            if (i == len) {
                out += CHARS.charAt(c1 >> 2);
                out += CHARS.charAt((c1 & 0x3) << 4);
                out += "==";
                break;
            }
            c2 = str.charCodeAt(i++);
            if (i == len) {
                out += CHARS.charAt(c1 >> 2);
                out += CHARS.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
                out += CHARS.charAt((c2 & 0xF) << 2);
                out += "=";
                break;
            }
            c3 = str.charCodeAt(i++);
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
            out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
            out += CHARS.charAt(c3 & 0x3F);
        }
        return out;
    },

    errorCallback(errorResponse, appController) {
        if (isForbiddenError(errorResponse)) {
            appController.disableLoading();
            appController.enableForbiddenError("Usuário não tem permissão para realizar esta operação.", "block");
        }

        if (isServerError(errorResponse)) {
            appController.disableLoading();
            appController.enableForbiddenError("Algo deu errado. Por favor tente novamente ou nos contate se o problema persistir.", "error");
        }
    }

});
