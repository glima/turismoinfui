import Ember from 'ember';
import { jwt_decode } from 'ember-cli-jwt-decode';

export default Ember.Mixin.create({
    session: Ember.inject.service('session'),

    administrator: false,
    intergado: false,
    
    isAdministrator() {
        if(this.get('session.data.authenticated.token')) {
            let perfil = jwt_decode(this.get('session.data.authenticated.token')).scopes[0];
            
            if(perfil == 'ROLE_ADMINISTRADOR' || perfil == 'ROLE_INTERGADO') {
                return true;
            } else {
                return false;
            } 
        } else {
            return false;
        }          
    },

    isIntergado() {
        if(this.get('session.data.authenticated.token')) {
            let perfil = jwt_decode(this.get('session.data.authenticated.token')).scopes[0];
            if(perfil == 'ROLE_INTERGADO') {
                return true;
            } else {
                return false;
            } 
        } else {
            return false;
        }          
    }
});
