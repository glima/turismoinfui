import Ember from 'ember';

export default Ember.Mixin.create({

    timeValidation: [{
      message: 'Horário inválido',
      validate: (inputValue) => {
          if(inputValue) {
              let timePattern = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
              return timePattern.test(inputValue);
          }

          return true;
      }
    }],

    emailValidation: [{
        message: 'Preencha o email com um formato válido',
        validate: (inputValue) => {
            if(inputValue) {
                let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                return emailPattern.test(inputValue);
            }

            return true;
        }
    }],

    floatValidation: [{
        message: 'Valor com formato inválido',
        validate: (inputValue) => {
            if(inputValue) {
                let pattern = /(\d|,)/;
                return pattern.test(inputValue);
            }

            return true;
        }
    }],

    scoreValidation: [{
        message: 'Escore deve ser entre 1 e 5',
        validate: (score) => {
            if(score) {
                return (score > 0 && score <= 5);
            }

            return true;
        }
    }],

    effectivenessValidation: [{
        message: 'Efetividade deve ser entre 0% e 100%',
        validate: (effectiveness) => {
            if(effectiveness) {
                return (effectiveness >= 0 && effectiveness <= 100);
            }

            return true;
        }
    }]
});
