import Ember from 'ember';

export default Ember.Mixin.create({

    parseStringDateToBr(date) {
        var year  = date.substring(0, 4);
        var month = date.substring(5, 7);
        var day = date.substring(8, 10);
        
        return day + "/" + month + "/" + year;
    },

    parseStringDateFromBr(date) {
        var day  = date.substring(0, 2);
        var month = date.substring(3, 5);
        var year = date.substring(6, 10);
        
        return year + "-" + month + "-" + day;
    },

    parseStringDateNoDash(date) {
        return date.replace(/-/g, '');
    },

    parseDate(date) {
        var year  = date.substring(0, 4);
        var month = date.substring(5,7);
        var day = date.substring(8,10);
        
        var format = year + "-" + month + "-" + day;
        
        var newDate = new Date(format + "T00:00:00-03:00");
        return newDate;
    },

    parseDateToString(date) {
        var year  = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        
        if(month.toString().length == 1) {
            month = "0" + month;
        }
        
        if(day.toString().length == 1) {
            day = "0" + day;
        }
        
        return year + "-" + month + "-" + day;
    }
});
