import Ember from 'ember';
import Parse from 'parse';
import Request from 'request';

export default Ember.Mixin.create(Parse, Request, {
    appController: Ember.inject.controller('app'),
    
    queryParams: ['search', 'page'],
    search: null,
    limit: 15,
    limitOptions: [15],
    pages: [1],
    page: 1,
    totalPages: 15,
    url: '',

    filterDialog: false,
    filter: null,
    log: null, 

    init() {
        this._super();
        this.resetFilter();
    },

    resetFilter() {
        this.set('filter', {});
        this.set('search', null);
        this.set('page', 1);
        this.set('log', null);
    },

    makeRequest() {
        var context = this;
        let contextApp = this.get('appController');   
        contextApp.enableLoading(); 
        if(this.get('search') == null) {
            this.ajaxGet(this.get('url') + '?page=' + this.get('page'), context)
            .then((response) => {
                contextApp.disableLoading();
                context.set('model', response);
                context.set('totalPages', response.total);
            });
        } else {
            this.ajaxGet(this.get('url') + '?page=' + this.get('page') 
                + '&search=' + this.get('search'), context)
            .then((response) => {
                contextApp.disableLoading();
                context.set('model', response);
                context.set('totalPages', response.total);
            });
        }
    },

    actions: {

        showLog(log) {
            this.set('log', log);
        },

        incrementPage() {
            if(this.get('page') < this.get('totalPages') / 15) {
                this.set('page', this.get('page') + 1);
                this.makeRequest();
            }            
        },

        decrementPage() {
            if(this.get('page') != 1) {
                this.set('page', this.get('page') - 1);
                this.makeRequest();
            }
        },
        
        openDialogSearch() {
            this.toggleProperty('filterDialog');
            this.resetFilter();
        },

        requestFilter() {
            let filter = '';
            let property = this.get('filter');
            let keys = Object.keys(property)

            for(let i = 0; i < keys.length; i++) {
                let value = property[keys[i]].value;
                if(value) {
                    if(property[keys[i]].type == 'date') {
                        filter += keys[i] + property[keys[i]].op + this.parseStringDateNoDash(value) + ",";
                    } else {
                        filter += keys[i] + property[keys[i]].op + value + ",";
                    }                    
                }                
            }

            if(filter != '') {
                filter = filter.substring(0, filter.length - 1);
            }

            this.resetFilter();

            if(filter == '') {
                this.set('search', null);
            } else {
                this.set('search', filter);
            }

            this.makeRequest();
            this.toggleProperty('filterDialog');
        }
    }
});
