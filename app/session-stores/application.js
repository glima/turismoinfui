import AdaptiveStore from 'ember-simple-auth/session-stores/adaptive';
import ENV from '../config/environment';

export default AdaptiveStore.extend({
    cookieDomain: ENV.serverUrl,
    cookiePath: '/' + ENV.modulePrefix,
    cookieName: 'lida-elite-session-cookie',
    localStorageKey: 'lida-elite-session-cookie'
});