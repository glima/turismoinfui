import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {

  this.route('app', { path: '/' }, function() {
    this.route('users', function() {
      this.route('new');
      this.route('edit', { path: '/:user_id' });
    });

    this.route('categories', function() {
      this.route('new');
      this.route('edit', { path: '/:category_id' });
    });

    this.route('places', function() {
      this.route('new');
      this.route('edit', { path: '/:place_id' });
    });

    this.route('configuration', function() {});

    this.route('events', function() {
      this.route('new');
      this.route('edit', { path: '/:event_id' });
    });
  });

  this.route('account', { path: '/account' }, function() {
    this.route('login');
  });
});

export default Router;
