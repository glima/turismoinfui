import Ember from 'ember'
import ENV from '../config/environment';

export default Ember.Component.extend({
    rootUrl: ENV.rootURL,
    session: Ember.inject.service('session'),
    authenticator: 'authenticator:custom',
    loading: false,

    keyPress: function(event) {
        if(event.which === 13) {
            this.$().find('button').click();
        }
    },

    actions: {
        authenticate: function() {
            this.set('loading', true);
            var credentials = this.getProperties('identification', 'password');

            this.get('session').authenticate('authenticator:custom', credentials)
            .then((response) => {
                this.set('loading', false);

                return response;
            })
            .catch((message) => {
                this.set('loading', false);
                this.set('errorMessage', message);
            });
        }
    }
});
