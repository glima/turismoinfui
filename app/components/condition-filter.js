import Ember from 'ember'

export default Ember.Component.extend({

    type: 'text',

    items: { ':': 'Igual','>': 'Maior','<': 'Menor', '~': 'Contém' },
    dateSelections: [':','>','<'],
    textSelections: [':', '~'],
    
    optionSelected: null,

    labelOption: Ember.computed('optionSelected', 'items', function() {
        return this.get('items')[this.get('optionSelected')];
    }),

    selections: Ember.computed('type', {
        get() {
            let type = this.get('type');
            if(type == 'date' || type == 'number') {
                return this.get('dateSelections');
            } else {
                return this.get('textSelections');
            }
        }
    })
});
