import Ember from 'ember';
import {isNotFoundError, isBadRequestError} from 'ember-ajax/errors';
import Request from '../../../mixins/request';
import UserAccess from '../../../mixins/user-access';

export default Ember.Controller.extend(Request, UserAccess, {
    appController: Ember.inject.controller('app'),

    dialog: {show: false, msg: '', title: ''},
    hasErrors: true,
    showConfirmDialog: false,
    category: null,

    clearUser() {
        this.set('category', null);
    },

    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i].title + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }
    },

    actions: {
        newCategory() {
            this.transitionToRoute('app.categories.new');
        },

        editCategory(category) {
            this.transitionToRoute('app.categories.edit', category.id);
        },

        deleteCategory() {
            let context = this.get('appController');
            context.enableLoading();
            this.ajaxDelete('/categories/' + this.get('category').id, context)
                .then((response) => {
                    context.disableLoading();
                    this.clearUser();
                    this.set('showConfirmDialog', false);
                    this.send('invalidateModel'); // recarrega tabela
                    this.showDialogScreen("Categoria excluída com sucesso", false);
                }).catch((error) => {
                    context.disableLoading();
                    if(isNotFoundError(error)) {
                        this.set('showConfirmDialog', false);
                        this.showDialogScreen("Categoria não encontrada", false);
                    }

                    if(isBadRequestError(error)) {
                        this.set('showConfirmDialog', false);
                        this.showDialogScreen(error.errors, true);
                    }
                });
        },

        confirmDialogDeleteCategory(category) {
            this.set('category', category);
            this.set('showConfirmDialog', true);
        },

        cancelConfirmDeleteCategory() {
            this.clearUser();
            this.set('showConfirmDialog', false);
        },

        closeDialog() {
            this.set('dialog.show', false);
        }
    }
});
