import Ember from 'ember';
import {isAjaxError, isNotFoundError, isForbiddenError, isBadRequestError} from 'ember-ajax/errors';
import Request from '../../../mixins/request';
import ENV from '../../../config/environment';

export default Ember.Controller.extend(Request, {
    appController: Ember.inject.controller('app'),

    dialog: {show: false, msg: '', title: ''},
    category: {},
    hasErrors: true,
    upload: false,
    _uploader: null,
    errors: [],

    resetFields() {
        this.set("category", {});
    },

    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i].title + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }
    },

    newCategory(category) {
      let context = this.get('appController');
      context.enableLoading();

      const that = this;
      this.set('errors', []);
      this._uploader.upload({
        url: ENV.serverUrl + 'api/adm/categories',
        data: {
          category: JSON.stringify(category)
        }
      }).then((payload) => {
          context.disableLoading();

          this.resetFields();
          this.showDialogScreen("Categoria salva com sucesso.", false);
        },
        (response) => {
          context.disableLoading();

          if (response.message) {
            that.set('errors', [response.message, null, 'Something happen']);
          }

          if(response.status == 400) {
            this.showDialogScreen(response.message, true);
          } else {
            context.enableForbiddenError("Algo deu errado. Por favor tente novamente ou nos contate se o problema persistir.", "error");
          }
        }
      );
    },

    editCategory(category) {
      let context = this.get('appController');
      context.enableLoading();

      const that = this;
      this.set('errors', []);
      this._uploader.upload({
        url: ENV.serverUrl + 'api/adm/categories/' + category.id,
        data: {
          category: JSON.stringify(category)
        }
      }).then((payload) => {
          this.resetFields();
          this.showDialogScreen("Categoria editada com sucesso.", false);
        },
        (response) => {
          if (response.message) {
            that.set('errors', [response.message, null, 'Something happen']);
          }

          if(response.status == 400) {
            this.showDialogScreen(response.message, true);
          } else {
            context.enableForbiddenError("Algo deu errado. Por favor tente novamente ou nos contate se o problema persistir.", "error");
          }
        }
      );
    },

    getBlob(imageURL) {
      var block = imageURL.split(";");
      // Get the content type of the image
      var contentType = block[0].split(":")[1];// In this case "image/gif"
      // get the real base64 content of the file
      var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

      // Convert it to a blob to upload
      var blob = this.b64toBlob(realData, contentType);
      return blob;
    },

    b64toBlob(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    },

    actions: {
        addCategory() {
            let category = this.get("category");

            if(category.id) {
                this.editCategory(category);
            } else {
                this.newCategory(category);
            }
        },

        cancel() {
            this.resetFields();
            this.transitionToRoute('app.categories');
        },

        closeDialog() {
            this.set('dialog.show', false);

            if(!this.get('hasErrors')) {
                this.transitionToRoute('app.categories');
            }
        },

        uploaderInit: function (uploader) {
          this._uploader = uploader;

          if(this.get('category').image) {
            this._uploader.addFiles([this.getBlob(this.get('category').image)]);
          }
        }
    }
});
