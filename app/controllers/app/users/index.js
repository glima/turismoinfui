import Ember from 'ember';
import {isNotFoundError, isBadRequestError} from 'ember-ajax/errors';
import Request from '../../../mixins/request';
import UserAccess from '../../../mixins/user-access';

export default Ember.Controller.extend(Request, UserAccess, {
    appController: Ember.inject.controller('app'),

    dialog: {show: false, msg: '', title: ''},
    hasErrors: true,
    showConfirmDialog: false,
    user: null,

    clearUser() {
        this.set('user', null);
    },

    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i].title + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }            
    },

    updateStatusUser(id, status) {
        let context = this.get('appController');   
        context.enableLoading(); 
        let user = { status: status };

        this.ajaxPut('/user/' + id + "/status", { data: JSON.stringify(user) }, context)
            .then((response) => {
                context.disableLoading();
                this.send('invalidateModel'); // recarrega tabela
                this.showDialogScreen("Situação do usuário atualizada com sucesso", false);
            }).catch((error) => {
                context.disableLoading();
                if(isNotFoundError(error)) {
                    this.set('showConfirmDialog', false);
                    this.showDialogScreen("Usuário não encontrado", false);
                }
                
                if(isBadRequestError(error)) {
                    this.set('showConfirmDialog', false);
                    this.showDialogScreen(error.errors, true);
                }
            });
    },

    actions: {
        newUser() {
            this.transitionToRoute('app.users.new');
        },

        editUser(user) {
            this.transitionToRoute('app.users.edit', user.id);
        },

        deleteUser() {
            let context = this.get('appController');   
            context.enableLoading(); 
            this.ajaxDelete('/user/' + this.get('user').id, context)
                .then((response) => {
                    context.disableLoading();
                    this.clearUser();
                    this.set('showConfirmDialog', false);
                    this.send('invalidateModel'); // recarrega tabela
                    this.showDialogScreen("Usuário excluído com sucesso", false);
                }).catch((error) => {
                    context.disableLoading();
                    if(isNotFoundError(error)) {
                        this.set('showConfirmDialog', false);
                        this.showDialogScreen("Usuário não encontrado", false);
                    }
                    
                    if(isBadRequestError(error)) {
                        this.set('showConfirmDialog', false);
                        this.showDialogScreen(error.errors, true);
                    }
                });
        },

        disableUser(id) {
            this.updateStatusUser(id, false);
        },

        enableUser(id) {
            this.updateStatusUser(id, true);
        },

        confirmDialogDeleteUser(user) {
            this.set('user', user);
            this.set('showConfirmDialog', true);
        },

        cancelConfirmDeleteUser() {
            this.clearUser();
            this.set('showConfirmDialog', false);
        },

        closeDialog() {
            this.set('dialog.show', false);
        }
    }
});
