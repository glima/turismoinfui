import Ember from 'ember';
import {isAjaxError, isNotFoundError, isForbiddenError, isBadRequestError} from 'ember-ajax/errors';
import Validation from '../../../mixins/validation';
import Request from '../../../mixins/request';

export default Ember.Controller.extend(Validation, Request, {
    appController: Ember.inject.controller('app'),

    dialog: {show: false, msg: '', title: ''},
    user: {},
    hasErrors: true,
    roles: ["ADMINISTRADOR", "TECNICO"],

    resetFields() {
        this.set("user", {});
    },

    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i].title + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }
    },

    newUser(user) {
        let context = this.get('appController');
        context.enableLoading();
        this.ajaxPost('/user', { data: JSON.stringify(user) }, context)
            .then((response) => {
                if(response) {
                    context.disableLoading();
                    this.resetFields();
                    this.showDialogScreen("Usuário salvo com sucesso.", false);
                }
            }).catch((error) => {
                context.disableLoading();
                if(isBadRequestError(error)) {
                    this.showDialogScreen(error.errors, true);
                    return;
                }
            });
    },

    editUser(user) {
        let context = this.get('appController');
        context.enableLoading();
        this.ajaxPut('/user/' + user.id, { data: JSON.stringify(user) }, context)
            .then((response) => {
                if(response) {
                    context.disableLoading();
                    this.resetFields();
                    this.showDialogScreen("Usuário editado com sucesso.", false);
                }
            }).catch((error) => {
                context.disableLoading();
                if(isBadRequestError(error)) {
                    this.showDialogScreen(error.errors, true);
                    return;
                }

                if(isNotFoundError(error)) {
                    this.showDialogScreen("Usuário não encontrado", false);
                }
            });
    },

    actions: {
        addUser() {
            let user = this.get("user");

            if(user.id) {
                this.editUser(user);
            } else {
                this.newUser(user);
            }
        },

        cancel() {
            this.resetFields();
            this.transitionToRoute('app.users');
        },

        closeDialog() {
            this.set('dialog.show', false);

            if(!this.get('hasErrors')) {
                this.transitionToRoute('app.users');
            }
        }
    }
});
