import Ember from 'ember';
import {isAjaxError, isNotFoundError, isForbiddenError, isBadRequestError} from 'ember-ajax/errors';
import Request from '../../../mixins/request';
import ENV from '../../../config/environment';
import Validation from '../../../mixins/validation';

export default Ember.Controller.extend(Validation, Request, {
    appController: Ember.inject.controller('app'),

    dialog: {show: false, msg: '', title: ''},
    event: {},
    hasErrors: true,
    showItems: false,
    upload: false,
    _uploader: null,
    errors: [],

    resetFields() {
        this.set("event", {});
    },

    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i] + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }
    },

    newEvent(event) {
      let context = this.get('appController');
      context.enableLoading();

      const that = this;
      this.set('errors', []);
      this._uploader.upload({
        url: ENV.serverUrl + 'api/adm/events',
        data: {
          event: JSON.stringify(event)
        }
      }).then((payload) => {
          context.disableLoading();

          this.resetFields();
          this.showDialogScreen("Evento salvo com sucesso.", false);
        },
        (response) => {
          context.disableLoading();

          if (response.message) {
            that.set('errors', [response.message, null, 'Something happen']);
          }

          if(response.status == 400) {
            this.showDialogScreen(response.response, true);
          } else {
            context.enableForbiddenError("Algo deu errado. Por favor tente novamente ou nos contate se o problema persistir.", "error");
          }
        }
      );
    },

    editEvent(event) {
      let context = this.get('appController');
      context.enableLoading();

      const that = this;
      this.set('errors', []);
      this._uploader.upload({
        url: ENV.serverUrl + 'api/adm/events/' + event.id,
        data: {
          event: JSON.stringify(event)
        }
      }).then((payload) => {
          this.resetFields();
          this.showDialogScreen("Evento editado com sucesso.", false);
        },
        (response) => {
          context.disableLoading();
          if (response.message) {
            that.set('errors', [response.message, null, 'Something happen']);
          }

          if(response.status == 400) {
            this.showDialogScreen(response.response, true);
          } else {
            context.enableForbiddenError("Algo deu errado. Por favor tente novamente ou nos contate se o problema persistir.", "error");
          }
        }
      );
    },

    getBlob(imageURL) {
      var block = imageURL.split(";");
      // Get the content type of the image
      var contentType = block[0].split(":")[1];// In this case "image/gif"
      // get the real base64 content of the file
      var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

      // Convert it to a blob to upload
      var blob = this.b64toBlob(realData, contentType);
      return blob;
    },

    b64toBlob(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    },

    actions: {
        getAddress() {
          let cep = this.get('event').cep;
          let ctx = this;

          Ember.$.ajax({
            type: 'GET',
            url: 'https://webmaniabr.com/api/1/cep/' + cep +'/?app_key=OBjcKrHMYzS9qcZPWUs1c3CXMTVMCSuy&app_secret=NfwInNeK48mKvCFW0AKDj0ANxFF50WgnMJRhhFgUQBmdMc5P',
            contentType: false,
            processData: false,
            error: function (jqXHR) {
              var error = jqXHR.responseText;
              try {
                error = JSON.parse(error);
              } catch (e) {
                error = new Ember.Error(error);
              }
              console.log(error);
            },
            success: function (data) {
              ctx.set('event.street', data.endereco);
              ctx.set('event.city', data.cidade);
            }
          });
        },

        addEvent() {
            let event = this.get("event");

            if(event.id) {
                this.editEvent(event);
            } else {
                this.newEvent(event);
            }
        },

        cancel() {
            this.resetFields();
            this.transitionToRoute('app.events');
        },

        closeDialog() {
            this.set('dialog.show', false);

            if(!this.get('hasErrors')) {
                this.transitionToRoute('app.events');
            }
        },

        showDialog() {
          let ctx = this;
          let selectedChange = [];

          let optionsSelected = this.get("event.categories");
          let optionsList = this.get("model.categories");

          if(optionsList && optionsSelected) {
              let options = optionsList._result || optionsList;

              options.forEach((elementOptions) => {
                  optionsSelected.forEach(elementSeleted => {
                      if(elementOptions.id == elementSeleted.id) {
                          selectedChange.push(elementOptions);
                      }
                  }, ctx);
              }, ctx);

              this.set('event.categories', selectedChange);
          }

          this.set('showItems', true);
        },

        closeDialogCategories() {
            this.set('showItems', false);
        },

        uploaderInit: function (uploader) {
          this._uploader = uploader;

          if(this.get('event').images) {
            let files = [];
            this.get('event').images.forEach(element => {
               files.push(this.getBlob(element));
            });

            this._uploader.addFiles(files);
          }
        }
    }
});
