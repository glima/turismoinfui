import DS from 'ember-data';
import Ember from 'ember';
import AjaxService from 'ember-ajax/services/ajax';

export default AjaxService.extend({
    ajax: Ember.inject.service(),
    
    headers: Ember.computed('session.token',function() {
        return {
            'Accept': 'application/json',
            'Authorization': 'Bearer ${this.get("session.token")}'
        };
    })
});
