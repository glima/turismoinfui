import Ember from 'ember';
import AjaxService from 'ember-ajax/services/ajax';
import {isAjaxError, isNotFoundError, isForbiddenError, isUnauthorizedError} from 'ember-ajax/errors';
import ENV from '../config/environment';

export default AjaxService.extend( {

    session: Ember.inject.service('session'),
    routing: Ember.inject.service('-routing'),

    host: ENV.serverUrl + 'api/adm',
    contentType: 'application/json; charset=utf-8',

    request(url, options) {
        let context = this;

        return this._super(url, options)
        .then((response) => {

            return response;
        })
        .catch(function(error) {
            if(isUnauthorizedError(error)) {
                //context.get('session').invalidate();
            }

            throw error;
        });
    }
});
