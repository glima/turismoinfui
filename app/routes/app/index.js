import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../mixins/load';

export default Ember.Route.extend(Load, {

    beforeModel() {
        this._super();
        this.changeTitle('Informações Turísticas');
    }
});
