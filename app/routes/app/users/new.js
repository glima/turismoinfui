import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    beforeModel() {
        this._super();
        this.changeTitle('Usuário', 'user-primary');
    },

    resetController(controller, isExiting, transition) {
        if (isExiting) {
            controller.resetFields();
        }
    }
});
