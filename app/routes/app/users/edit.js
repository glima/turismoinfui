import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    beforeModel() {
        this._super();
        this.changeTitle('Usuário', 'user-primary');
    },

    renderTemplate(controller, model) {
        let newController = this.controllerFor('app/users/new');
        this._super(newController, model);

        // seleciona as fazendas que o usuário possui
        if(model.user.farms) {
            for(let i = 0; i < model.farms.length; i++) {
                if(this.hasFarmUser(model.farms[i].id, model.user.farms)) {
                    model.farms[i].enabled = true;
                }
            }
        }

        // seta model para o controller
        newController.set("user", model.user);
        newController.set("model", model);
        this.render('app/users/edit', { controller: newController });
    },

    hasFarmUser(idFarm, farms) {
        let has = false;
        for(let i = 0; i < farms.length; i++) {
            if(farms[i].id == idFarm) {
                has = true;
                i = farms.length;
            }
        }

        return has;
    },

    model(params) {
        let appController = this.controllerFor('app');
        return Ember.RSVP.hash({
            user: this.ajaxGet('/user/' + params.user_id, appController)
        });
    },

    resetController(controller, isExiting, transition) {
        if (isExiting) {
            this.controllerFor('app/users/new').resetFields();
        }
    }
});
