import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    model() {
        return this.ajaxGet('/user', this.controllerFor('app'));
    },

    afterModel() {
        this._super();
        this.changeTitle('Usuário', 'user-primary');
    },

    actions: {
        invalidateModel: function() {
            this.refresh();
        }
    }
});
