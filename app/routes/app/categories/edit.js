import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    beforeModel() {
        this._super();
        this.changeTitle('Categoria', 'category-primary');
    },

    renderTemplate(controller, model) {
        let newController = this.controllerFor('app/categories/new');
        this._super(newController, model);

        let category = model;

        // seta model para o controller
        newController.set("category", category);
        newController.set("model", model);
        this.render('app/categories/new', { controller: newController });
    },

    model(params) {
        return this.ajaxGet('/categories/' + params['category_id'], this.controllerFor('app'));
    },

    resetController(controller, isExiting, transition) {
        if (isExiting) {
            this.controllerFor('app/categories/new').resetFields();
        }
    }
});
