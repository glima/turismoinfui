import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';

export default Ember.Route.extend(Load, {

    beforeModel() {
        this._super();
        this.changeTitle('Categoria', 'category-primary');
    },

    resetController(controller, isExiting, transition) {
        if (isExiting) {
            controller.resetFields();
            controller.set('hasErrors', true);
            controller.set('dialog', {show: false, msg: '', title: ''});
        }
    }
});
