import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    beforeModel() {
        this._super();
        this.changeTitle('Local', 'warehouse-primary');
    },

    renderTemplate(controller, model) {
        let newController = this.controllerFor('app/places/new');
        this._super(newController, model);

        let place = model.place;

        // seta model para o controller
        newController.set("place", place);
        newController.set("model", model);
        this.render('app/places/new', { controller: newController });
    },

    model(params) {
      return Ember.RSVP.hash({
          place: this.ajaxGet('/places/' + params['place_id'], this.controllerFor('app')),
          categories: this.ajaxGet('/categories/', this.controllerFor('app'))
      });
    },

    resetController(controller, isExiting, transition) {
        if (isExiting) {
            this.controllerFor('app/places/new').resetFields();
        }
    }
});
