import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    beforeModel() {
        this._super();
        this.changeTitle('Place', 'warehouse-primary');
    },

    model(params) {
      return Ember.RSVP.hash({
        categories: this.ajaxGet('/categories/', this.controllerFor('app'))
      });
    },

    resetController(controller, isExiting, transition) {
        if (isExiting) {
            controller.resetFields();
            controller.set('hasErrors', true);
            controller.set('dialog', {show: false, msg: '', title: ''});
        }
    }
});
