import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    afterModel() {
        this._super();
        this.changeTitle('Configuração', 'config-primary');
    },

    renderTemplate(controller, model) {
      this._super(controller, model);

      let conf = model;

      // seta model para o controller
      controller.set("conf", conf);
      controller.set("model", model);
      this.render('app/configuration/index', { controller: controller });
    },

    model() {
        return this.ajaxGet('/configurations', this.controllerFor('app'));
    },

    actions: {
        invalidateModel: function() {
            this.refresh();
        }
    }
});
