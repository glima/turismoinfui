import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    afterModel() {
        this._super();
        this.changeTitle('Evento', 'flag-primary');
    },

    model() {
        return this.ajaxGet('/events', this.controllerFor('app'));
    },

    actions: {
        invalidateModel: function() {
            this.refresh();
        }
    }
});
