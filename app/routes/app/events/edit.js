import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Load from '../../../mixins/load';
import Request from '../../../mixins/request';

export default Ember.Route.extend(Load, Request, {

    beforeModel() {
        this._super();
        this.changeTitle('Local', 'flag-primary');
    },

    renderTemplate(controller, model) {
        let newController = this.controllerFor('app/events/new');
        this._super(newController, model);

        let event = model.event;

        // seta model para o controller
        newController.set("event", event);
        newController.set("model", model);
        this.render('app/events/new', { controller: newController });
    },

    model(params) {
      return Ember.RSVP.hash({
          event: this.ajaxGet('/events/' + params['event_id'], this.controllerFor('app')),
          categories: this.ajaxGet('/categories/', this.controllerFor('app'))
      });
    },

    resetController(controller, isExiting, transition) {
        if (isExiting) {
            this.controllerFor('app/events/new').resetFields();
        }
    }
});
