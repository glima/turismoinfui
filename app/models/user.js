import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr(),
    login: DS.attr(),
    email: DS.attr(),
    password: DS.attr(),
    phone: DS.attr()
});
