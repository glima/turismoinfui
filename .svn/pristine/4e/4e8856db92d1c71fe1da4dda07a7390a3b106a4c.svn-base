import Ember from 'ember';
import ENV from '../config/environment';
import {isAjaxError, isNotFoundError, isForbiddenError, isBadRequestError} from 'ember-ajax/errors';
import { jwt_decode } from 'ember-cli-jwt-decode';
import Request from '../mixins/request';
import UserAccess from '../mixins/user-access';

export default Ember.Controller.extend(Request, UserAccess, {
    rootUrl: ENV.rootURL,

    title: 'Lida Web',
    titleIcon: null,
    leftSideBarLockedOpen: true,
    submenu: {  
        routineList: false,
        routineDown: false,
        manejoList: false,
        manejoDown: false,
        areaList: false,
        areaDown: false,
        drugstoreList: false,
        drugstoreDown: false
    },    
    loading: false,
    loadingPasswd: false,
    alterPassword: false,
    user: { password: '', currentPassword: '', passwordConfirm: '' },
    dialog: {show: false, msg: '', title: ''},
    dialogError: {show: false, msg: ''},
    userName: '',
    perfil: '',

    defineUserName() {
        if(this.get('session.data.authenticated.token')) {            
            this.set('userName', jwt_decode(this.get('session.data.authenticated.token')).user.name);
            this.set('perfil', jwt_decode(this.get('session.data.authenticated.token')).scopes[0].replace('ROLE_', ''));
        }        
    },

    enableForbiddenError(msg, icon) {
        this.set('dialogError.show', true);
        this.set('dialogError.msg', msg);
        this.set('dialogError.icon', icon);
    },

    enableLoading(key) {
        if(key) {
            this.set(key, true);
        } else {
            this.set('loading', true);
        }
    },

    disableLoading(key) {
        if(key) {
            this.set(key, false);
        } else {
            this.set('loading', false);
        }
    },

    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i].title + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }            
    },

    changeTitle(title, icon) {
        this.set('title', title);
        this.set('titleIcon', icon);
    },

    cleanAlterPassword() {
        this.set('alterPassword', false);
        this.set('user', { password: '', currentPassword: '', passwordConfirm: '' });
    },

    actions: {

        closeDialog() {
            this.set('dialog.show', false);
            this.set('dialogError.show', false);
        },

        requestAlterPassword() {
            if(this.get('user').newPassword != this.get('user').passwordConfirm) {
                this.showDialogScreen([{ title: 'Senha e confirmação de senha não conferem.'}], true);
            } else { 
                this.enableLoading('loadingPasswd'); 
                this.ajaxPut('/user/password', { data: JSON.stringify(this.get('user')) }, this)
                    .then((response) => {
                        if(response) {
                            this.disableLoading('loadingPasswd');
                            this.showDialogScreen("Senha alterada com sucesso.", false);
                            this.cleanAlterPassword();
                        }
                    }).catch((error) => {
                        this.disableLoading('loadingPasswd');
                        if(isBadRequestError(error)) {
                            this.showDialogScreen(error.errors, true);
                            return;
                        }

                        if(isNotFoundError(error)) {
                            this.showDialogScreen("Usuário não encontrado", true);
                        }
                    });
            }
        },

        setAlterPassword() {
            this.set('alterPassword', true);
        },

        cancelAlterPassword() {
            this.cleanAlterPassword();
        },

        invalidateSession() {
            this.get('session').invalidate();
        },

        toggle(value) {
            this.toggleProperty(value);
        },

        goAnimalRoutines() {
            this.transitionToRoute('app.animal_routines');
        },

        goSaleRoutines() {
            this.transitionToRoute('app.sale_routines');
        },

        goAnimalCategories() {
            this.transitionToRoute('app.animal_categories');
        },

        goHealthcareRoutines() {
            this.transitionToRoute('app.healthcare_routines');
        },

        goManejoRoutines() {
            this.transitionToRoute('app.combined_routines');
        },

        goGrasses() {
            this.transitionToRoute('app.grasses');
        },

        goAreaServices() {
            this.transitionToRoute('app.area_services');
        },

        goModules() {
            this.transitionToRoute('app.modules');
        },
        
        goFavoriteProduct() {
            this.transitionToRoute('app.piquets');
        },

        goPiquets() {
            this.transitionToRoute('app.piquets');
        },

        goExternalFarms() {
            this.transitionToRoute('app.external_farms');
        },

        goLots() {
            this.transitionToRoute('app.lots');
        },

        goGtas() {
            this.transitionToRoute('app.gtas');
        },

        openArea() {
            this.toggleProperty('submenu.areaList');
            this.toggleProperty('submenu.areaDown');
        },
        
        openDrugstore() {
            this.toggleProperty('submenu.drugstoreList');
            this.toggleProperty('submenu.drugstoreDown');
        },

        openManejo() {
            this.toggleProperty('submenu.manejoList');
            this.toggleProperty('submenu.manejoDown');
        },

        openRoutine() {
            this.toggleProperty('submenu.routineList');
            this.toggleProperty('submenu.routineDown');
        },

        goUsers() { 
            this.transitionToRoute('app.users');
        },

        goAnimals() { 
            this.transitionToRoute('app.process.animals');
        },

        goCategoryChanges() { 
            this.transitionToRoute('app.process.categories_change');
        },

        goLotChanges() {
            this.transitionToRoute('app.process.lots_change');
        },

        goTagChanges() { 
            this.transitionToRoute('app.process.tags_change');
        },

        goDeaths() { 
            this.transitionToRoute('app.process.deaths');
        },

        goSales() { 
            this.transitionToRoute('app.process.sales');
        },
        
        goAnimalsRejects() {
            this.transitionToRoute('app.process.animals_reject');
        },

        goPasturages() { 
            this.transitionToRoute('app.process.pasturages');
        },

        goHealthcare() { 
            this.transitionToRoute('app.process.healthcare');
        },

        goWeighing() { 
            this.transitionToRoute('app.process.weighings');
        },

        goOccurrences() {
            this.transitionToRoute('app.process.occurrences');
        },

        goVisualProfiles() {
            this.transitionToRoute('app.process.visual_profiles');
        },

        goEtrEpfs() {
            this.transitionToRoute('app.process.etr_epfs');
        },

        goFarms() { 
            this.transitionToRoute('app.farms');
        },

        goCompanies() { 
            this.transitionToRoute('app.companies');
        }
    }
});
