import Ember from 'ember';
import {isAjaxError, isNotFoundError, isForbiddenError, isBadRequestError} from 'ember-ajax/errors';
import Request from '../../../mixins/request';

export default Ember.Controller.extend(Request, {
    appController: Ember.inject.controller('app'),

    dialog: {show: false, msg: '', title: ''},
    lot: {},
    hasErrors: true,

    resetFields() {
        this.set("lot", {});
    },

    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i].title + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }            
    },

    newLot(lot) {
        let context = this.get('appController');   
        context.enableLoading(); 

        this.ajaxPost('/lot', { data: JSON.stringify(lot) }, context)
            .then((response) => {
                if(response) {
                    context.disableLoading();

                    this.resetFields();
                    this.showDialogScreen("Lote salvo com sucesso.", false);
                }
                
            }).catch((error) => {
                context.disableLoading();

                if(isBadRequestError(error)) {
                    this.showDialogScreen(error.errors, true);
                    return;
                }
            });
    },

    editLot(lot) {
        let context = this.get('appController');
        context.enableLoading(); 

        this.ajaxPut('/lot/' + lot.id, { data: JSON.stringify(lot) }, context)
            .then((response) => {
                if(response) {
                    context.disableLoading();
                    this.resetFields();
                    this.showDialogScreen("Lote editado com sucesso.", false);
                }
            }).catch((error) => {
                context.disableLoading();
                if(isBadRequestError(error)) {
                    this.showDialogScreen(error.errors, true);
                    return;
                }

                if(isNotFoundError(error)) {
                    this.showDialogScreen("Lote não encontrado", false);
                }
            });
    },

    actions: {
        addLot() {
            let lot = this.get("lot");
            
            if(lot.id) {
                this.editLot(lot);
            } else {
                this.newLot(lot);
            }
        },

        cancel() {
            this.set("lot", { units: []});
            this.transitionToRoute('app.lots');
        },

        closeDialog() {
            this.set('dialog.show', false);

            if(!this.get('hasErrors')) {
                this.transitionToRoute('app.lots');
            }
        }
    }
});
