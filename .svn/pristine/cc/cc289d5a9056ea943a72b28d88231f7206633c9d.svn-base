import Ember from 'ember';
import {isAjaxError, isNotFoundError, isForbiddenError, isBadRequestError} from 'ember-ajax/errors';
import Request from '../../../mixins/request';

export default Ember.Controller.extend(Request, {
    appController: Ember.inject.controller('app'),

    routine: {},
    dialog: {show: false, msg: '', title: ''},
    
    resetFields() {
        this.set("routine", {});
    },

    showDialogScreen(msgs, error) {
        this.set('dialog.show', true);
        this.set('hasErrors', error);
        let msg = "";

        if(error) {
            for(let i = 0; i < msgs.length; i++) {
                msg += msgs[i].title + '<br>';
            }

            this.set("dialog.title", "Aviso");
            this.set("dialog.msg", Ember.String.htmlSafe(msg));
        } else {
            this.set("dialog.title", "Sucesso");
            this.set("dialog.msg", Ember.String.htmlSafe(msgs));
        }            
    },
    
    actions: {
        copyRoutine() {
            let routine = this.get("routine");
            let context = this.get('appController');   
            context.enableLoading(); 
    
            this.ajaxPost('/healthcare_routine/' + routine.id + '/copy', { data: JSON.stringify(routine) }, context)
                .then((response) => {
                    if(response) {
                        context.disableLoading();
                        this.resetFields();
                        this.showDialogScreen("Protocolo copiado com sucesso.", false);
                    }
                }).catch((error) => {
                    context.disableLoading();
                    if(isBadRequestError(error)) {
                        this.showDialogScreen(error.errors, true);
                        return;
                    }
    
                    if(isNotFoundError(error)) {
                        this.showDialogScreen("Protocolo não encontrado", false);
                    }
                });
        },

        cancel() {
            this.resetFields();
            this.transitionToRoute('app.healthcare_routines');
        },
        
        closeDialog() {
            this.set('dialog.show', false);

            if(!this.get('hasErrors')) {
                this.transitionToRoute('app.healthcare_routines');
            }
        }
    }
});
